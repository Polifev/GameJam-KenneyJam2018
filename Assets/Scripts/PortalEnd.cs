﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalEnd : MonoBehaviour {

    ParticleSystem particleSystem;
    AudioSource audioSource;

    [SerializeField] ParticleSystem Endlevel;
    [SerializeField] AudioClip Endlevel_Clip;
    [SerializeField] bool IsTransition = false;

    // Use this for initialization
    void Start () {
        particleSystem = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        Endlevel.Play();
        audioSource.PlayOneShot(Endlevel_Clip);
        Invoke("LoadNextLevel", 1f);
    }

    void LoadNextLevel()
    {
        if(IsTransition)
        {
            SceneManager.LoadScene(SceneTransitionManager.GetNextRoom());
            ScoreManager.ResetTimer();
        }
        else
        {
            SceneManager.LoadScene("Transition");
        }
        /*int currentScene = SceneManager.GetActiveScene().buildIndex;
        currentScene += 1;
        if (currentScene == SceneManager.sceneCountInBuildSettings)
        {
            currentScene = 0;
        }
        SceneManager.LoadScene(currentScene);*/
    }
}
