﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class OptionsMenu : MonoBehaviour
{
    [SerializeField] AudioMixer mixer;

    public void OnClickBack()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void SetVolume(float value)
    {
        mixer.SetFloat("Volume", value);
    }
}
