﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTransitionManager
{
    private static List<string> RoomNames;
    public static int CurrentRoom = 0;

    static SceneTransitionManager()
    {
        RoomNames = new List<string>();
        RoomNames.Add("Level_prehistoire");
        RoomNames.Add("level_MoyenAge");
        RoomNames.Add("Level_sea");
        RoomNames.Add("Level_futurist");
        RoomNames.Add("Credits");
    }
    
    public static string GetNextRoom()
    {
        string Next = RoomNames[CurrentRoom];
        if (CurrentRoom < RoomNames.Count)
        {
            CurrentRoom++;
        }
        else
        {
            CurrentRoom = 0;
        }
        return Next;
    }
}
