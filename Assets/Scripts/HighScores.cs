﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public class HighScores : MonoBehaviour
{
    List<int> Score;

	// Use this for initialization
	void Start ()
    {
        Score = new List<int>();
        LoadHighScores();
        CompareHighScores();
        PrintHighScores();
        UpdateHighScores();
	}

    void LoadHighScores()
    {
        StreamReader reader = new StreamReader("highscores");
        string content = reader.ReadToEnd();
        print(content);
        string[] scores = content.Split(';');
        foreach(string s in scores)
        {
            Score.Add(int.Parse(s));
        }
        reader.Close();
    }

    void CompareHighScores()
    {
        int myScore = ScoreManager.CurrentScore;
        for (int i = 0; i < Score.Count; i++)
        {
            if(myScore > Score[i])
            {
                Score.Insert(i, myScore);
                break;
            }
        }
    }

    void PrintHighScores()
    {
        for (int i = 0; i < 3; i++)
        {
            TextMeshProUGUI[] txt = GetComponentsInChildren<TextMeshProUGUI>();
            txt[i].text = (i+1) + ": " + Score[i];
        }
    }

    void UpdateHighScores()
    {
        StreamWriter w = new StreamWriter("highscores", false);
        string list = "";
        for (int i = 0; i < 3; i++)
        {
            list += Score[i];
            if(i != 2)
            {
                list += ";";
            }
        }
        ScoreManager.ResetScore();
        w.Write(list);
        w.Close();
    }
}
