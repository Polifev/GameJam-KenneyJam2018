﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorSpawner : MonoBehaviour {

    [SerializeField] Vector3 Direction;
    [SerializeField] float Interval;
    [SerializeField] float Speed;
    [SerializeField] GameObject Prefab;

    private float Timer;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Timer += Time.deltaTime;
        if(Timer > Interval)
        {
            Timer -= Interval;
            SpawnMeteor();
        }
	}

    void SpawnMeteor ()
    {
        GameObject instance = GameObject.Instantiate(Prefab, transform);
        instance.GetComponent<Rigidbody>().AddForce(Vector3.Normalize(Direction - transform.position) * Speed);
    }
}
