﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject cam;
    [SerializeField] Vector3 offset;
    [SerializeField] float magnitude;

    public bool isShaking = false;

    // Use this for initialization
    void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        updatePosition();
    }

    void updatePosition()
    {
        float dX = transform.position.x - cam.transform.position.x + offset.x;
        float dY = transform.position.y - cam.transform.position.y + offset.y;
        float dZ = transform.position.z - cam.transform.position.z + offset.z;
        CameraShaking(ref dX,ref dY,ref dZ);
        cam.transform.Translate(dX, dY, dZ, Space.World);
    }

    void CameraShaking(ref float dX,ref float dY,ref float dZ)
    {
        if (isShaking)
        {
            dX += Random.Range(-1, 1) * magnitude;
            dY += Random.Range(-1, 1) * magnitude;
            dZ += Random.Range(-1, 1) * magnitude;
        }
    }
}