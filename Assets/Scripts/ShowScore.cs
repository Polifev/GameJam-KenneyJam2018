﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowScore : MonoBehaviour
{
    TextMeshProUGUI txt;
	void Start ()
    {
        txt = GetComponent<TextMeshProUGUI>();
        ScoreManager.CalculateScore();
        txt.text = "Score: " + ScoreManager.CurrentScore;
	}
}
