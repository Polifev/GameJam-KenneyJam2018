﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour
{
    ParticleSystem particleSystem;


    [Header("Particles")]
    [SerializeField] ParticleSystem explosionParticles;
    [SerializeField] ParticleSystem explosionSmokeParticles;

    // Use this for initialization
    void Start ()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    void OnCollisionEnter(Collision collision)
    {
        explosionParticles.Play();
        explosionSmokeParticles.Play();
        GetComponent<AudioSource>().Play();
        Destroy(GetComponent<MeshCollider>());
        GetComponent<Renderer>().enabled = false;
        Invoke("destroyObject", 5f);
    }

    private void destroyObject()
    {
        Destroy(gameObject);
    }
}
