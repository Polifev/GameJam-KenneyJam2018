﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager
{
    public static int CurrentScore;
    public static float Timer;

    static ScoreManager()
    {
        ResetScore();
        ResetTimer();
    }

    public static void ResetScore()
    {
        CurrentScore = 0;
    }

    public static void ResetTimer()
    {
        Timer = 0f;
    }

    public static void AddTimeToTimeCount(float dt)
    {
        Timer += dt;
    }

    public static void CalculateScore()
    {
        CurrentScore += Mathf.RoundToInt((30f - Timer) * 100f);
    }

    public static void PickupBonus()
    {
        CurrentScore += 1500;
    }
}
