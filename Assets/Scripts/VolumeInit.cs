﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeInit : MonoBehaviour {

    // Use this for initialization
    [SerializeField] AudioMixer mixer;
	void Start ()
    {
        Slider s = GetComponent<Slider>();
        float v;
        mixer.GetFloat("Volume", out v);
        s.value = v;
	}
}
