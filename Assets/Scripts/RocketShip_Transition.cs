﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketShip_Transition : MonoBehaviour {

    Rigidbody rigidBody;
    AudioSource audioSource;
    ParticleSystem particleSystem;

    //Init var:
    [SerializeField] int speed;
    
    [SerializeField] AudioClip thrust_audio;
    [SerializeField] AudioClip finishlevel_audio;

    [SerializeField] ParticleSystem thrustFireParticles;
    [SerializeField] ParticleSystem thrustSmokeParticles;

    CameraFollow cameraFollow;

    public enum State { Alive, Dead, Menu, Rewind}
    public State state = State.Alive;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        particleSystem = GetComponent<ParticleSystem>();
        cameraFollow = GetComponentInParent<CameraFollow>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == State.Alive)
        {
            InputResponse();
        }
    }

    void InputResponse()
    {
        rigidBody.AddRelativeForce(Vector3.back * speed);
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(thrust_audio);
        }
        thrustFireParticles.Play();
        thrustSmokeParticles.Play();
    }

    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Finish":
                audioSource.Stop();
                audioSource.PlayOneShot(finishlevel_audio);
                Destroy(gameObject);
                break;
            default:
                break;
        }
    }
}
