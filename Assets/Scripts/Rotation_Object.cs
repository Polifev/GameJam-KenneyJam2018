﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation_Object : MonoBehaviour {

    [SerializeField] float speedRotation;
    [SerializeField] float xRotation;
    [SerializeField] float yRotation;
    [SerializeField] float zRotation;

    //Quaternion rotation;
    Vector3 rotation;

    // Use this for initialization
    void Start () {
        //rotation = Quaternion.Euler(xRotation, yRotation, zRotation);
        rotation = new Vector3(xRotation, yRotation, zRotation);
    }
	
	// Update is called once per frame
	void Update () {
        //transform.localRotation = rotation;
        //transform.Rotate(rotation * Time.deltaTime * speedRotation, Space.Self);
        transform.Rotate(rotation, speedRotation * Time.deltaTime);
    }
}
