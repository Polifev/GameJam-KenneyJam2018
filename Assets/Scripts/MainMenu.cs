﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public RocketShip_Transition rocketShip;

    void Start()
    {
        rocketShip = GameObject.Find("SpaceCraft_transition").GetComponent<RocketShip_Transition>();
        rocketShip.state = RocketShip_Transition.State.Menu;
        SceneTransitionManager.CurrentRoom = 0;
    }

    public void OnClickPlay()
    {
        rocketShip.state = RocketShip_Transition.State.Alive;
    }

    public void OnRejouer()
    {
        SceneManager.LoadScene("Level_prehistoire");
        SceneTransitionManager.CurrentRoom += 1;
    }

    public void OnClickHowto()
    {
        SceneManager.LoadScene("Howto");
    }

    public void OnClickOptions()
    {
        SceneManager.LoadScene("OptionsMenu");
    }

    public void OnClickMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void OnClickExit()
    {
        Application.Quit();
    }

}
