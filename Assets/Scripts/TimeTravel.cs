﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTravel : MonoBehaviour
{
    List<Vector3> oldPositions;
    List<Quaternion> oldRotations;

    void Start()
    {
        oldPositions = new List<Vector3>();
        oldRotations = new List<Quaternion>();
    }

    public void Record(Vector3 Position, Quaternion Rotation)
    {
        oldPositions.Insert(0, Position);
        oldRotations.Insert(0, Rotation);
        int max = Mathf.RoundToInt( 3f / Time.fixedDeltaTime );
        if(oldRotations.Count > max)
        {
            oldPositions.RemoveAt(oldPositions.Count - 1);
            oldRotations.RemoveAt(oldRotations.Count - 1);
        }
    }

    public int Rewind()
    {
        transform.position = oldPositions[0];
        transform.rotation = oldRotations[0];
        oldPositions.RemoveAt(0);
        oldRotations.RemoveAt(0);
        if(oldRotations.Count > 0)
        {
            oldPositions.RemoveAt(0);
            oldRotations.RemoveAt(0);
        }
        return oldPositions.Count;
    }
}
