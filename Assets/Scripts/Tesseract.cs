﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tesseract : MonoBehaviour
{
    float rocketShipHeight;

    // Use this for initialization
    void Start ()
    {
        GameObject rocketShip;
        rocketShip = GameObject.Find("SpaceCraft");
        rocketShipHeight = rocketShip.transform.position.y;
        Vector3 newPosition = transform.position;
        newPosition.y = rocketShipHeight;
        transform.position = newPosition;
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }

}