﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.PostProcessing;

public class RocketshipJB : MonoBehaviour {

    Rigidbody rigidBody;
    AudioSource audioSource;
    ParticleSystem particleSystem;
    TimeTravel timeTravel;
    CameraFollow cameraFollow;
    GameObject playerCamera;
    PostProcessingBehaviour postProcess;

    public enum State { Alive, Dead, Rewind, Menu}
    public State state = State.Alive;

    [Header("Movement")]
    [SerializeField] int speed;
    [SerializeField] int speedRotation;
    [SerializeField] int lateralSpeed;
    [SerializeField] float frictionCoefficient;
    [SerializeField] float shakeDuration;

    [Header("Audio")]
    [SerializeField] AudioClip thrust_audio;
    [SerializeField] AudioClip rewind_audio;
    [SerializeField] AudioClip death_audio;
    [SerializeField] AudioClip bonus_audio;

    [Header("Particles")]
    [SerializeField] ParticleSystem thrustFireParticles;
    [SerializeField] ParticleSystem thrustSmokeParticles;
    [SerializeField] ParticleSystem deathExplosionParticles;
    [SerializeField] ParticleSystem deathDebrisParticles;
    [SerializeField] ParticleSystem deathSparkParticles;

    public Vector3 currentLocation;

    // Use this for initialization
    void Start ()
    {
        ScoreManager.ResetTimer();
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        particleSystem = GetComponent<ParticleSystem>();
        cameraFollow = GetComponentInParent<CameraFollow>();
        timeTravel = GetComponentInParent<TimeTravel>();
        playerCamera = GameObject.Find("PlayerCamera");
        postProcess = playerCamera.GetComponent<PostProcessingBehaviour>();
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        ScoreManager.AddTimeToTimeCount(Time.fixedDeltaTime);
        if (state == State.Alive)
        {
            SetChromaticAberrationValue(0.024f);
            SetVignetteIntensity(0.4f);
            InputResponse();
            Friction();
            timeTravel.Record(transform.position, transform.rotation);
        }
        if (state == State.Rewind)
        {
            SetChromaticAberrationValue(1);
            SetVignetteIntensity(0.45f);
            HandleRewind();
        }
        //informations pour d'autres script
        currentLocation = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    private void SetChromaticAberrationValue(float value)
    {
        ChromaticAberrationModel.Settings model = postProcess.profile.chromaticAberration.settings;
        model.intensity = value;
        postProcess.profile.chromaticAberration.settings = model;
    }

    private void SetVignetteIntensity(float value)
    {
        VignetteModel.Settings model = postProcess.profile.vignette.settings;
        model.intensity = value;
        postProcess.profile.vignette.settings = model;
    }

    private void HandleRewind()
    {
        audioSource.PlayOneShot(rewind_audio);
        int count = timeTravel.Rewind();
        if (count == 0 || Input.GetKeyUp(KeyCode.Space))
        {
            rigidBody.isKinematic = false;
            state = State.Alive;
            audioSource.Stop();
            Quaternion q = Quaternion.identity;
            transform.rotation = q;
            transform.Rotate(0, 180, 0);
        }
    }

    void InputResponse()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rigidBody.isKinematic = true;
            audioSource.Stop();
            state = State.Rewind;
            thrustFireParticles.Stop();
            thrustSmokeParticles.Stop();
            return;
        }


        if (Input.GetKey(KeyCode.UpArrow))
        {
            rigidBody.AddRelativeForce(Vector3.back * speed);
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(thrust_audio);
            }
            thrustFireParticles.Play();
            thrustSmokeParticles.Play();
        }
        else
        {
            audioSource.Stop();
            thrustFireParticles.Stop();
            thrustSmokeParticles.Stop();
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            rigidBody.AddRelativeForce(-Vector3.back * speed * 2);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigidBody.AddRelativeForce(Vector3.right * lateralSpeed);
            if (transform.localEulerAngles.z >= 300 || transform.localEulerAngles.z <= 65)
            {
                transform.Rotate(-Vector3.forward * Time.deltaTime * speedRotation);
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rigidBody.AddRelativeForce(Vector3.left * lateralSpeed);
            if (transform.localEulerAngles.z <= 60 || transform.localEulerAngles.z >= 295)
            {
                transform.Rotate(Vector3.forward * Time.deltaTime * speedRotation);
            }
        }
    }

    void Friction()
    {
        Vector3 friction = rigidBody.velocity * frictionCoefficient * Time.deltaTime;
        rigidBody.AddRelativeForce(friction);
    }

    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Finish":
                if(state == State.Alive)
                {
                    audioSource.Stop();
                    Destroy(gameObject);
                }
                break;
            default:
                if (state == State.Alive) // S'il est déjà mort, il est inutile de tout refaire
                {
                    state = State.Dead;
                    audioSource.Stop();
                    thrustFireParticles.Stop();
                    thrustSmokeParticles.Stop();
                    MakeCameraShake();
                    audioSource.PlayOneShot(death_audio);
                    deathExplosionParticles.Play();
                    deathDebrisParticles.Play();
                    deathSparkParticles.Play();
                    rigidBody.freezeRotation = false;
                    Invoke("ReloadLevel", 4f);
                }
                break;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        audioSource.PlayOneShot(bonus_audio);
        ScoreManager.PickupBonus();
    }

    void MakeCameraShake()
    {
        cameraFollow.isShaking = true;
        Invoke("StopCameraShake", shakeDuration);
    }

    void StopCameraShake()
    {
        cameraFollow.isShaking = false;
    }

    void ReloadLevel()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;;
        SceneManager.LoadScene(currentScene);
    }
}

